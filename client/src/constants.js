var moment = require('moment');

// function to get next_monday and next_tuesday for exercise.
const dayINeed = 1;
let next_monday = null;
if (moment().isoWeekday() <= dayINeed) { next_monday = moment().isoWeekday(dayINeed); } else { next_monday = moment().add(1, 'weeks').isoWeekday(dayINeed); }

const fromDate = next_monday.format("YYYY-MM-DD");
const next_tuesday = next_monday.add(1, 'days');
const toDate = next_tuesday.format("YYYY-MM-DD");

// constants for exercise.
export const CLASS = 'Economy'
export const ADULTS = 2
export const FROMPLACE = 'EDI'
export const TOPLACE = 'LON'
export const PARAMS = { adults: ADULTS, class: CLASS, toPlace: 'LOND-sky', toDate, fromPlace: FROMPLACE, fromDate  };

// Redux
export const FETCH_REQUEST = 'FETCH_REQUEST'
export const FETCH_SUCCESS = 'FETCH_SUCCESS'
export const FETCH_ERROR = 'FETCH_ERROR'

export const MESSAGE_ERROR = 'Ops.. something went wrong! Is the server on? :)'
export const MESSAGE_LOADING = 'Loading data, wait a few seconds'
