import React from 'react';

import BpkLargeBurgherIcon from 'bpk-component-icon/lg/menu';

import './TopNav.scss';
import logo from '../..//skyscanner_logo.svg';


const TopNav = () => (
  <header className='header'>
    <a href="/">
      <span className='logoText'>Skyscanner</span>
      <img className='logo' alt="Skyscanner" src={logo}/>
    </a>
     <BpkLargeBurgherIcon className="iconBlue"/>
  </header>
);

export default TopNav;
