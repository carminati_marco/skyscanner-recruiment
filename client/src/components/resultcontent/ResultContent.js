import React, { Component } from 'react';
import './ResultContent.scss';
import BpkCard from 'bpk-component-card';
import { BpkGridContainer } from 'bpk-component-grid';
import ResultContentLeg from './ResultContentLeg.js';
import ResultContentFooter from './ResultContentFooter.js'
import LazyLoad from 'react-lazy-load';


class ResultContent extends Component {
  render()
  {
    const itineraries = this.props.itineraries;
    return (
      <div className="contentDiv">
        {itineraries.map((itinerary, k) => {
          return (
            <LazyLoad key={k} height={228}>
              <BpkCard key={k} padded={false} className="contentCard">
                <BpkGridContainer >
                  <ResultContentLeg leg={itinerary.OutboundLeg} />
                  <ResultContentLeg leg={itinerary.InboundLeg}/>
                  <ResultContentFooter agent={itinerary.Agent} price={itinerary.Price} />
                </ BpkGridContainer>
              </BpkCard>
            </LazyLoad>
          );
        })}
      </div>
    )
  }
}

export default ResultContent;
