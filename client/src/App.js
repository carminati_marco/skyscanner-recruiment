import React, { Component } from 'react';
import './App.scss';
import { fetchItinerariesRequest, fetchItinerariesSuccess, fetchItinerariesError } from './actions'
import BpkBannerAlert, { ALERT_TYPES } from 'bpk-component-banner-alert';

import { connect } from 'react-redux';
import TopNav from './components/topnav';
import Header from './components/header';
import Placeholder from './components/placeholder';
import ResultContent from './components/resultcontent';

import { PARAMS, FROMPLACE, TOPLACE, ADULTS, CLASS, MESSAGE_LOADING, MESSAGE_ERROR } from './constants'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fromPlace: '',
      toPlace: '',
      adults: '',
      class_plane: '',
      itineraries: [],
      message: MESSAGE_LOADING,
      type_alert: ALERT_TYPES.WARN,
    }
  }
	componentDidMount(){
    // init sestate with constant.
    this.setState({fromPlace: FROMPLACE, toPlace: TOPLACE, adults:ADULTS, class_plane:CLASS });
    // fetch itineraries using redux.
  	this.props.fetchItinerariesWithRedux()
  }
  renderItineraries() {
    // function that renders 'itineraries' or shows a message.
    const { itineraries, message, type_alert } = this.props;
    if (itineraries) {
    return (
        <ResultContent itineraries={itineraries} />
      )
    } else {
      if (message && type_alert) {
        return (
          <div className="alert" >
            <BpkBannerAlert
              message={message} type={type_alert} />
          </div>
        )
      }
    }
  }
	render(){
	  return (
      <div className="App">
        <TopNav/>
          <Header fromPlace={this.state.fromPlace}  toPlace={this.state.toPlace}
            adults={this.state.adults} class_plane={this.state.class_plane} />
          <Placeholder  />
          {this.renderItineraries()}
      </div>
    )
  }
}

function fetchItinerariesWithRedux() {
  // manages fetching itineraries, success or error of call created with redux.
  // see action/index.js to more info about type.
	return (dispatch) => {
  	dispatch(fetchItinerariesRequest(MESSAGE_LOADING, ALERT_TYPES.WARN));
    return fetchItineraries(dispatch).then(([response, json]) =>{
    	if(response.status === 200){
      	dispatch(fetchItinerariesSuccess(json['itineraries'], ))
      }
      else{
      	dispatch(fetchItinerariesError(MESSAGE_ERROR, ALERT_TYPES.ERROR))
      }
    })
  }
}

function fetchItineraries(dispatch) {
  // it calls the server URL. (note: PARAMS are constants due the exercise.)
  var url = new URL("http://localhost:4000/api/search"),
      params = PARAMS
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  return fetch(url, { method: 'GET'})
     .then(response => Promise.all([response, response.json()]))
     .catch(error => dispatch(fetchItinerariesError(MESSAGE_ERROR, ALERT_TYPES.ERROR)) );
}

function mapStateToProps(state){
	return state
}

export default connect(mapStateToProps, { fetchItinerariesWithRedux })(App);
