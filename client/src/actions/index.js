import { FETCH_REQUEST, FETCH_SUCCESS, FETCH_ERROR } from '../constants';

// FETCH_REQUEST: starts the request and set message + type_alert for waiting
// FETCH_SUCCESS: set the itineraries
// FETCH_ERROR: manages the error and set message + type_alert

export function fetchItinerariesRequest(message, type_alert){
  return {
    type: FETCH_REQUEST,
    message, type_alert
  }
}

export function fetchItinerariesSuccess(itineraries) {
  return {
    type: FETCH_SUCCESS,
    itineraries
  }
}

export function fetchItinerariesError(message, type_alert) {
  return {
    type: FETCH_ERROR,
    message, type_alert
  }
}
