var request = require('request');

describe('Testing the service online', () => {
  test('It should response the GET method', (done) => {
    request.get('http://localhost:4000', {
        method: 'GET'
      },
      function(err, res, body) {
        expect(res.statusCode).toBe(200);
        done();
      });
  })
});

describe('Testing the api with a query', () => {
  test('It should response the GET method', (done) => {
    request.get("http://localhost:4000/api/search?adults=1&class=Economy&toPlace=EDI&toDate=2017-09-26&fromPlace=LHR&fromDate=2017-09-25", {
      timeout: 150000,
      method: 'GET'
    }, function(err, res, body) {
      expect(res.statusCode).toBe(200);
      done();
    });
  })
});
