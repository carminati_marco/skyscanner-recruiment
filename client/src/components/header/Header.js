import React, { Component } from 'react'
import BpkText from 'bpk-component-text';
import LongArrowRightIcon from 'bpk-component-icon/lg/long-arrow-right';
var pluralize = require('pluralize')

import './Header.scss';


class Header extends Component {
  render(){
  return (
  <div className="subheader">
    <BpkText textStyle="xxl">{this.props.fromPlace}</BpkText>
    <BpkText textStyle="xxl" ><LongArrowRightIcon className="iconWhite" /></BpkText>
    <BpkText textStyle="xxl">{this.props.toPlace}</BpkText>
    <br />
    <BpkText>{this.props.adults} {pluralize('traveller', this.props.adults)}, {this.props.class_plane}</BpkText>
  </div>
  )}
};

export default Header;
