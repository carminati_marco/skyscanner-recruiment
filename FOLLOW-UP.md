# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?
**redux** as state container and **redux-thunk** as middleware. With a state container
the project is easier to manage for further improvements (i.e. new actions with itineraries, etc). States of itineraries, type alert and text alert, which you can find in the project, can be an example.

**react-lazy-load**: to avoid rendering all itineraries.

**pluralize**: to allow managing strings in singular or plural using a number
(avoiding to insert 'if' in the code).

**react-time**: to format a datetime (**note**: as an alternative the format
  could be done in server, the same way I adopted for the time;
  I preferred to cast in client thinking to have the need also to format the date for further improvements).

Different components of the **bpk-component** package: they are useful to render objects in accordance with your guidelines.

### Q) What is the command to start the server?
I used expressjs to avoid installation by your side of other frameworks like django/django REST project.
So it's the Default command.
(Default) `APIKEY=<key> npm run server`

---

# General:

### Q) How long, in hours, did you spend on the test?

~4h for backend server + to transform json (I studied the json response and started to use expressjs).
~3h to study the backpack + to create frontend schema.
~3-4h to connect backend with server and to see firsts result and to make tests.
~1h to add redux + ~1 to test.
~2-3h to adjust the layout.
~1h to clean the code, to add comments, etc.

**note**: due to my full time job, I couldn't work continuously, so I aggregated the hours in steps
(surely to work continuously will give less hours).


### Q) If you had more time, what further improvements or new features would you add?
1. I see that the server sends a lot of data. I would create
a pagination/infinite scroll to avoid a big list as I obtain in the exercise; this will reduce the server timing.

2. I would add in the layout "from date" and "to date", they could be useful for UX.

3. I would manage the server's ValidationErrors in the client side (I didn't do as I used constants and those values are validated).

4. I would create a section for Segments that aren't displayed in the exercise
(**note**: this is the reason why server doesn't load this data in the transformer functions).

5. If the project should become bigger, I would review the css parts with same properties
(with a main css, for example App.scss, that contains the common properties).

### Q) Which parts are you most proud of? And why?
I really appreciate the ExpressJs; I usually create API usingn django + django REST and I found ExpressJs faster in development.
So I'd say the server part.
Then, in the frontend, I'm proud of the redux + redux-thunk integration.

### Q) Which parts did you spend the most time with? What did you find most difficult?
I had some problems to configure the middleware redux-thunk as I never used before (as it happens in our job, I spent time in searching and trying other middlewares before finding it).

### Q) How did you find the test overall? If you have any suggestions on how we can improve the test or our API, we'd love to hear them.
I had a couple of doubts during the development
-1. In the README.md there is the follow structure
>Itineraries
>  Legs
>    Segments

But Segments aren't displayed in the layout that is in the designs folder (in fact I adopted the choice not to fetch legs's data).

-2. The call creates a big list; in the README.md isn't explained if we have to create a pagination (as it isn't designed in the sketch) or an infinite-scroll, if we can use paginations' parameters in the call to fetch less data,
or if this doesn't matter.

Thank you so much for your time!
marco
