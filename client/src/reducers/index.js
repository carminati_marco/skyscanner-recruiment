import { FETCH_REQUEST, FETCH_SUCCESS, FETCH_ERROR } from '../constants';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return {...state, message: action.message, type_alert: action.type_alert};
    case FETCH_SUCCESS:
      return {...state, itineraries: action.itineraries};
    case FETCH_ERROR:
      return {...state, message: action.message, type_alert: action.type_alert};
    default:
      return state;
  }
}

export default reducer
