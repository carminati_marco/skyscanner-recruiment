import React, {Component} from 'react';
import Time from 'react-time';

import BpkText from 'bpk-component-text';
import AwardIcon from 'bpk-component-icon/sm/long-arrow-right';
import { BpkGridRow, BpkGridColumn } from 'bpk-component-grid';
import BpkImage from 'bpk-component-image';

import './ResultContent.scss';


class ResultContentLeg extends Component {
  render()
  {
    const leg = this.props.leg;
    return (
      <BpkGridRow padded={false} >
        <BpkGridColumn style={{padding:'20px'}}  width={2} tabletWidth={2}>
          <BpkImage style={{width:'32px',height:'32px'}} className="legImg"
            altText={leg.Carrier.Name} width={32} height={32} src={leg.FaviconCarrier} />
        </BpkGridColumn>
        <BpkGridColumn width={2} tabletWidth={2}>
          <BpkText className="legBaseBlack"><Time value={leg.Departure} format="HH:mm"/></BpkText><br/>
          <BpkText className="legBaseGray">{leg.OriginStation.Code}</BpkText>
        </BpkGridColumn>
        <BpkGridColumn className="legColumn" style={{height:'48px'}} width={1} tabletWidth={1}>
          <AwardIcon className="legIconGray" style={{height:'48px'}}  />
        </BpkGridColumn>
        <BpkGridColumn width={2} tabletWidth={2}>
          <BpkText ><Time value={leg.Arrival} format="HH:mm"/></BpkText><br/>
          <BpkText className="legBaseGray">{leg.DestinationStation.Code}</BpkText>
        </BpkGridColumn>
        <BpkGridColumn className="textRight" offset={2} width={3} tabletWidth={3}>
          <BpkText textStyle="sm" className="legBaseGray">{leg.Duration}</BpkText><br/>
          <BpkText textStyle="sm" className="legBaseGreen">{leg.StopLabel}</BpkText>
        </BpkGridColumn>
      </BpkGridRow>
    )
  }
}

export default ResultContentLeg;
