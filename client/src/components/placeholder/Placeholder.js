import React from 'react';
import './Placeholder.scss';

import BpkLink from 'bpk-component-link';
import BpkText from 'bpk-component-text';
import SearchRightIcon from 'bpk-component-icon/lg/price-alerts';
import { withAlignment } from 'bpk-component-icon';
import { lineHeightBase } from 'bpk-tokens/tokens/base.es6';
import { iconSizeLg } from 'bpk-tokens/tokens/base.es6';

const AlignedSearch = withAlignment(SearchRightIcon);

const AlignedSpan = withAlignment(
  'span', iconSizeLg, lineHeightBase
);

const Placeholder = () => (
  <div>
    <div className="placeholder">
      <BpkLink href="http://www.skyscanner.net/">
        <AlignedSpan >
        <BpkText className="placeholderTextBlueLeft" >
          Filter
        </BpkText>
      </AlignedSpan>
      </BpkLink>
      <BpkLink href="http://www.skyscanner.net/">
        <BpkText className="placeholderTextBlueLeft" >
          Sort
        </BpkText>
      </BpkLink>
      <BpkLink className="placeholderRight" href="http://www.skyscanner.net/">
        <AlignedSearch className="placeholderIconBlue" />
        <BpkText className="placeholderTextBlue" >
          Price alerts
        </BpkText>
      </BpkLink>
    </div>
  </div>
);

export default Placeholder;
