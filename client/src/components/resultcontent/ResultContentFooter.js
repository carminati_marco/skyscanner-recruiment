import React, { Component } from 'react';
import './ResultContent.scss';
import BpkText from 'bpk-component-text';
import { BpkGridRow, BpkGridColumn } from 'bpk-component-grid';
import BpkButton from 'bpk-component-button';

class ResultContentFooter extends Component {
  render()
  {
    return (
      <BpkGridRow className="footerGridRow">
        <BpkGridColumn padded={false} width={5} tabletWidth={5} >
          <div>
          <BpkText textStyle="xl">{this.props.symbol}{this.props.price}</BpkText>
          </div>
          <BpkText className="footerBaseGray">{this.props.agent}</BpkText>
        </BpkGridColumn>
        <BpkGridColumn className="textRight" offset={3} width={4} tabletWidth={4}>
          <BpkButton>Select</BpkButton>
        </BpkGridColumn>
      </BpkGridRow>
    )
  }
}

export default ResultContentFooter;
