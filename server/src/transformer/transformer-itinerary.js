var DataTransform = require("node-json-transform").DataTransform;
var humanizeDuration = require('humanize-duration')
var pluralize = require('pluralize')
var favicon = 'https://logos.skyscnr.com/images/airlines/favicon/%CODE.png'

var shortEnglishHumanizer = humanizeDuration.humanizer({
  // customized humanizeDuration adding value to cast Duration ad design.
  language: 'shortEn',
  conjunction: ' ',
  spacer: '',
  languages: {
    shortEn: {
      h: function() { return 'h' },
      m: function() { return '' },
    }
  }
});

function get_currency_symbol(codeCurrency, currencies) {
  // get the symbol of currencies to render.
  return currencies.find(s => s.Code === codeCurrency)['Symbol'];
}

function transform_legs(json) {
  // mapping legs;
  // it gets the data to render and it'll operate function if it needs (ex. Duration)

  var map_legs = {
      list : 'Legs',
      item: {
      	Id: "Id",
      	Duration: "Duration",
      	Departure: "Departure",
      	Arrival: "Arrival",
      	OriginStation: "OriginStation",
      	DestinationStation: "DestinationStation",
        SegmentIds: "SegmentIds",
        StopLabel: "Stops",
        Carrier: "Carriers.0",
        FaviconCarrier: "Carriers.0",
      },
      operate: [
      {
        // calculates the Duration.
		    run: function(val) { return shortEnglishHumanizer(val*60*1000)}, on: "Duration"
    	},
      {
        // get Carriers from json['Carriers'] (if there isn't the vaule in json, we get undefined)
		    run: function(val) { return json['Carriers'].find(s => s.Id === val)}, on: "Carrier"
    	},
      {
        // gets the Carrier's favicon
		    run: function(val) { return favicon.replace('%CODE', json['Carriers'].find(s => s.Id === val)['Code']) }, on: "FaviconCarrier"
    	},
    	{
        // calculates if the leg is direct or has stops.
  		  run: function(val) { if(val.length === 0) { return 'Direct'} else { return val.length + ' ' + pluralize('stop', val)}}, on: "StopLabel"
    	},
    	{
        // gets OriginStation from json['Places'] (if there isn't the vaule in json, we get undefined)
    		run: function(val) { return json['Places'].find(s => s.Id === val) }, on: "OriginStation"
    	},
    	{
        // gets DestinationStation from json['Places'] (if there isn't the vaule in json, we get undefined)
    		run: function(val) { return json['Places'].find(s => s.Id === val) }, on: "DestinationStation"
    	},
  	],
  };
  // transforms data and returns.
  var dataTransform = DataTransform(json, map_legs);
  return dataTransform.transform();
  };

function transform_itineraries(json) {
  // mapping itineraries;
  // it gets the data to render and it'll operate function if it needs (ex. Duration)

  // gets symbol and cast leglist
  symbol = get_currency_symbol(json['Query']['Currency'], json['Currencies']);
  legsList = transform_legs(json, symbol);

  var map_itineraries = {
      list : 'Itineraries',
      item: {
          Price: "PricingOptions.0.Price",
          Agent: "PricingOptions.0.Agents.0",
          OutboundLeg: "OutboundLegId",
          InboundLeg: "InboundLegId"
      },
      operate: [
      {
        // gets Agent from json['Agents'] (if there isn't the vaule in json, we get undefined)
		    run: function(val) { return json['Agents'].find(s => s.Id === val)['Name']}, on: "Agent"
      },
      {
        // gets the outbound leg.
  		  run: function(val) { return legsList.find(s => s.Id === val)}, on: "OutboundLeg"
      },
      {
        // gets the inbound leg.
        run: function(val) { return legsList.find(s => s.Id === val)}, on: "InboundLeg"
      },
      {
        // cast the price
        run: function(val) { return symbol + val  }, on: "Price"
      }
  	],
  };

  // transforms data and returns.
  var dataTransform = DataTransform(json, map_itineraries);
  itineraries = dataTransform.transform();
  return {itineraries};
};

// set module.exports
module.exports = {
  transformerItinerary: function (json) {
    return transform_itineraries(json);
  },
}
